/* -*-C-*-
// BPM synchronize routine

// BPM, a package manager for BLZ-OSPL
// Copyright (C) 2018 James Vaughan
//
// This program is free software: you can distribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the license, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

// For any questions, contact me at <dev.jamesvaughan@gmail.com>

// ----------------------------------------------------------------------------- */

/* IO */
#include <stdio.h>
/* access() */
#include <unistd.h>

/* Header for this file */
#include "synchronize.h"
/* Heartbeat Parser */
#include "parser.h"

/* Synchronizes packages with the lockfile and the manifest, in that order.
// The ideology followed is described by Sam Boyer in So You Want to Write a
// Package Manager. */
int
synchronize ()
{
  /* Holds Heartbeat metadata */
  struct Manifest Heartbeat = initializemanifest ();
  /* Reusable int for holding function results */
  int result;
  
  /* Check if Heartbeat file exists */
  if (access ("Heartbeat", R_OK) == -1)
    {
      /* -10: File not accessible */
      perror ("-10: Heartbeat file not accessible");
      
      return -10;
    }

  /* Parse Heartbeat file */
  result = parsehb (&Heartbeat);

  /* Check for errors */
  if (result != 0)
    {
      return result; /* parsehb */
    }

  /* Return success */
  return 0;
}
