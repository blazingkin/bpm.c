/* -*-C-*-
// Collection of string utilities

// BPM, a package manager for BLZ-OSPL
// Copyright (C) 2018 James Vaughan
//
// This program is free software: you can distribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the license, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

// For any questions, contact me at <dev.jamesvaughan@gmail.com>

// -----------------------------------------------------------------------------*/

/* isspace */
#include <ctype.h>
/* strlen */
#include <string.h>

/* Header for this file */
#include "bpmstring.h"

/* Trims trailing whitespace in-place */
int
trimtail (char* string)
{
  /* Pointer to end of string */
  char* end = string + strlen (string) - 1;

  /* Move pointer back */
  while (end > string && isspace (*end))
    {
      end--;
    }

  /* Write null-terminator */
  *(end + 1) = 0;

  return 0;
}
