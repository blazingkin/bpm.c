/* -*-C-*-
// Heartbeat parser for BPM

// BPM, a package manager for BLZ-OSPL
// Copyright (C) 2018 James Vaughan
//
// This program is free software: you can distribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the license, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

// For any questions, contact me at <dev.jamesvaughan@gmail.com>

// -----------------------------------------------------------------------------*/

/* IO */
#include <stdio.h>
/* isspace */
#include <ctype.h>
/* strncpy, strlen */
#include <string.h>
/* calloc */
#include <stdlib.h>
/* UCHAR_MAX */
#include <limits.h>

/* Header for this file */
#include "parser.h"
/* String utilities */
#include "bpmstring.h"

/* Initializes Manifest */
struct Manifest
initializemanifest ()
{
  struct Manifest manifest;

  /* Set default values */
  manifest.number = 0;
  manifest.metadata[0][0] = 0;
  manifest.metadata[1][0] = 0;
  manifest.metadata[2][0] = 0;

  /* Return initialized Manifest */
  return manifest;
}

/* Parses Heartbeat into INITIALIZED manifest struct */
int
parsehb (struct Manifest* manifest)
{
  /* Maximum line length for buffer */
  unsigned char BUFFER_LIMIT = UCHAR_MAX;
  
  /* Hearbeat file pointer */
  FILE* heartbeat_fp;
  /* String buffer */
  char* buffer;

  /* Open Heartbeat in Read mode */
  heartbeat_fp = fopen ("Heartbeat", "r");
  /* If file cannot be opened */
  if (heartbeat_fp == NULL)
    {
      /* -11: File open error */
      perror ("-11: Failed opening Heartbeat file");

      return -11;
    }

  /* Allocate zeroed memory for buffer */
  buffer = calloc (BUFFER_LIMIT, sizeof (*buffer));

  /* Unless there's an read error, parse */
  while (fgets (buffer, BUFFER_LIMIT, heartbeat_fp) != NULL)
    {
      /* Don't modify address of actual buffer pointer, to allow for freeing
         and to prevent segmentation faults. */
      char* start = buffer;
      
      /* Increment through whitespace */
      while (isspace ((unsigned char) *start))
	start++;

      /* Continue checking on next line if it's only whitespace */
      if (*start == 0)
        continue;

      /* Continue checking on next line if it's a comment (starts with #) */
      if (*start == '#')
        continue;

      /* Metadata starts with _ */
      if (*start == '_')
	{
	  /* Trim trailing whitespace */
	  trimtail (buffer);

	  /* Set package name if not already set */
	  if (*(start + 1) == 'P' && manifest->metadata[0][0] == 0)
	    strncpy (&(manifest->metadata[0][0]),
		     (start + 2), /* Ignore "_P" */
		     sizeof (manifest->metadata[0]));

	  /* Set package version if not already set */
	  if (*(start + 1) == 'V' && manifest->metadata[1][0] == 0)
	    strncpy (&(manifest->metadata[1][0]),
		     (start + 2), /* Ignore "_V" */
		     sizeof (manifest->metadata[1]));

	  /* Set package author if not already set */
	  if (*(start + 1) == 'A' && manifest->metadata[2][0] == 0)
	    strncpy (&(manifest->metadata[2][0]),
		     (buffer + 2), /* Ignore "_A" */
		     sizeof (manifest->metadata[2]));

	  /* TODO: Put in manual;
	  // It might seem like it would be a good idea to put a continue call
	  // in each if statement above, so that the program won't have to check
	  // whether (buffer + 1) is 'V' or 'A' if it already knows it's 'P'.
	  // It would save several cycles, sure, but to check if (buffer + 1) is
	  // 'A' it still has to check whether it is 'P' or 'V' anyways, so if
	  // that isn't being optimised in the same way (and I don't see how it
	  // could be) then what is the point of adding needless clutter?  Those
	  // lines would be reaching the point of diminishing returns between
	  // code readability and performance.
	  // - James Vaughan */
	  
	  /* Continue checking on next line */
	  continue;
	}

      /* Trim trailing whitespace and take it as a package by default */
      trimtail (buffer);
      strncpy (&(manifest->package[manifest->number][0]),
	       buffer,
	       sizeof (manifest->package[manifest->number]));
      /* Increment package number */
      manifest->number++;
    }

  /* Close file pointer */
  fclose (heartbeat_fp);

  /* Free allocated memory */
  free (buffer);

  /* Return success */
  return 0;
}
