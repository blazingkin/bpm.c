/* -*-C-*-
// Header for Heartbeat parser

// BPM, a package manager for BLZ-OSPL
// Copyright (C) 2018 James Vaughan
//
// This program is free software: you can distribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the license, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

// For any questions, contact me at <dev.jamesvaughan@gmail.com>

// -----------------------------------------------------------------------------*/

/* Include-guard */
#ifndef PARSER_H
#define PARSER_H
#endif

/* Describes Heartbeat file */
struct Manifest
{
  /* Number of packages */
  unsigned char number;
  /* Name, auther, license (max: 3, max-length: 80 + \0) */
  char metadata[03][81];
  /* Packages (max: 32, max-length: 80 + \0) */
  char package[32][81];
};

/* Initializes Manifest */
struct Manifest
initializemanifest ();  

/* Parses Heartbeat into INITIALIZED manifest struct */
int
parsehb (struct Manifest*);
